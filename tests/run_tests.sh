#!/bin/sh

export VIP_INCLUDE="192.168.1.101"
export VIP_EXCLUDE="192.168.1.102"

echo -ne "[debug]: setting up config file ... "
envsubst '${VIP_INCLUDE},${VIP_EXCLUDE}' < ./keepalived.conf.in > /etc/keepalived/keepalived.conf
if [ $? -ne 0 ] ; then
    echo "FAIL"
    exit 1
else
    echo "OK"
fi

echo -ne "[debug]: starting service ... "
systemctl start keepalived
if [ $? -ne 0 ] ; then
    echo "FAIL"
    exit 1
else
    echo "OK"
fi

echo -ne "[debug]: checking service active ... "
systemctl -q is-active keepalived
if [ $? -ne 0 ] ; then
    echo "FAIL"
    exit 1
else
    echo "OK"
fi

sleep 5

echo -ne "[debug]: checking included VIP ... "
ip addr show eth0 | grep -q ${VIP_INCLUDE}
if [ $? -ne 0 ] ; then
    echo "FAIL"
    exit 1
else
    echo "OK"
fi

echo -ne "[debug]: checking excluded VIP ... "
ip addr show eth0 | grep -q ${VIP_EXCLUDE}
if [ $? -ne 0 ] ; then
    echo "FAIL"
    exit 1
else
    echo "OK"
fi

echo -ne "[debug]: stopping service ... "
systemctl stop keepalived
if [ $? -ne 0 ] ; then
    echo "FAIL"
    exit 1
else
    echo "OK"
fi

echo -ne "[debug]: checking service inactive ... "
systemctl -q is-active keepalived
if [ $? -ne 3 ] ; then
    echo "FAIL"
    exit 1
else
    echo "OK"
fi

sleep 5

echo -ne "[debug]: checking include VIP ... "
ip addr show eth0 | grep -q ${VIP_INCLUDE}
if [ $? -ne 1 ] ; then
    echo "FAIL"
    exit 1
else
    echo "OK"
fi

echo -ne "[debug]: checking exclude VIP ... "
ip addr show eth0 | grep -q ${VIP_EXCLUDE}
if [ $? -ne 1 ] ; then
    echo "FAIL"
    exit 1
else
    echo "OK"
fi

exit 0
